/*
DiceRoll - By William Joseph Buggy (Some Color Mage)

Let's face it, random numbers are easy. Very easy. However, some people (like me) are too lazy to even type the code for that out every time.
That's what DiceRoll is for. It's a simple RNG module that can generate dice rolls in formats that you'd see in common tabletop games (i.e. of the format [x]d[y]+[z]).
There's also optional console text output if you want to see what's going on for testing purposes.

This little project is free to use, distribute, and edit for other purposes. All I ask is that you continue to attribute me in some form.
*/

#ifndef _DICEROLL_H_
#define _DICEROLL_H_

#include <cstdlib>
#include <iostream>
#include <ctime>
using namespace std;

class DiceRoll {
	public:
	DiceRoll();
	DiceRoll(bool);
	int OneRoll(int);
	int OneRollMod(int, int);
	int MultiRoll(int, int);
	int MultiRollMod(int, int, int);
	private:
	bool textLog;
};

#endif