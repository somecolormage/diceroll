/*
DiceRoll - By William Joseph Buggy (Some Color Mage)

Let's face it, random numbers are easy. Very easy. However, some people (like me) are too lazy to even type the code for that out every time.
That's what DiceRoll is for. It's a simple RNG module that can generate dice rolls in formats that you'd see in common tabletop games (i.e. of the format [x]d[y]+[z]).
There's also optional console text output if you want to see what's going on for testing purposes.

This little project is free to use, distribute, and edit for other purposes. All I ask is that you continue to attribute me in some form.
*/

#include "diceroll.h"
using namespace std;

//DiceRoll()
//Default constructor. Sets the seed for dice rolls, and assumes console output is not wanted.
DiceRoll::DiceRoll(){
	srand(time(NULL));
	
	textLog = false;
}

//DiceRoll(bool)
//Constructor. Sets the seed for dice rolls, and sets up console output if requested.
//
//logInit - true if you want console output, otherwise false
DiceRoll::DiceRoll(bool logInit){
	srand(time(NULL));
	
	textLog = logInit;
}

//OneRoll(int)
//Rolls a single die, and outputs result.
//
//y - number of sides on the die.
int DiceRoll::OneRoll (int y){
	int out = 0;
	
	if(textLog) cout << "Rolling one die, d" << y << "." << endl;
	
	out = rand() % y + 1;
	
	if(textLog) cout << "Rolled [" << out << "]" << endl;
	
	return out;
}

//OneRollMod(int)
//Rolls a single die, adds a given value, and outputs result.
//
//y - number of sides on the die.
//z - modifier added after dice roll.
int DiceRoll::OneRollMod (int y, int z){
	int out = 0;
	
	if(textLog) cout << "Rolling one die, d" << y << ", modifier " << z << "." << endl;
	
	out = rand() % y + 1;
	
	if(textLog) cout << "Rolled [" << out << "]" << endl;
	
	out += z;
	
	if(textLog) cout << "Final result [" << out << "]" << endl;
	
	return out;
}

//MultiRoll(int, int)
//Rolls multiple dice, adds together and outputs result.
//MultiRoll and MultiRollMod contain an int[] of the rolls, for error checking purposes.
//
//x - number of dice to roll.
//y - number of sides on the dice.
int DiceRoll::MultiRoll (int x, int y){
	int out = 0;
	int rolls[x];
	
	if(textLog){
		cout << "Rolling " << x << " dice, d" << y << "." << endl;
		cout << "Rolled [";
	}
	
	for(int i = 0; i < x; i++){
		rolls[i] = rand() % y + 1;
		out += rolls[i];
		if(textLog){
			cout << rolls[i];
			if(i < x - 1) cout << ", ";
		}
	}
	
	if(textLog){
		cout << "]" << endl;
		cout << "Final result [" << out << "]" << endl;
	}
	
	return out;
}

//MultiRollMod(int, int, int)
//Rolls multiple dice, adds together and outputs result.
//MultiRoll and MultiRollMod contain an int[] of the rolls, for error checking purposes.
//
//x - number of dice to roll.
//y - number of sides on the dice.
int DiceRoll::MultiRollMod (int x, int y, int z){
	int out = 0;
	int rolls[x];
	
	if(textLog){
		cout << "Rolling " << x << " dice, d" << y << ", modifier " << z << "." << endl;
		cout << "Rolled [";
	}
	
	for(int i = 0; i < x; i++){
		rolls[i] = rand() % y + 1;
		out += rolls[i];
		if(textLog){
			cout << rolls[i];
			if(i < x - 1) cout << ", ";
		}
	}
	
	if(textLog){
		cout << "]" << endl;
		cout << "Sum [" << out << "]" << endl;
	}
	
	out += z;
	
	if(textLog) cout << "Final result [" << out << "]" << endl;
	
	return out;
}